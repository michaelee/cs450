-module(basics).
-compile(export_all).

% basic pattern matching
factorial(0) -> 1;
factorial(N) -> N * factorial(N-1).

% guards -- note: guard expressions are limited!
grade(100) -> 'Perfect!';
grade(X) when X >= 90 -> 'A';
grade(X) when X >= 80 -> 'B';
grade(X) when X >= 70 -> 'C';
grade(X) when X >= 60 -> 'D';
grade(_) -> 'E'.

% if expression -- one guard must evaluate to true
max(A,B) -> if A < B -> B;                     
               true  -> A
            end.

% atoms and tuples
area({circle, R})       -> 3.1415 * R * R;
area({rectangle, L, W}) -> L * W;
area({square, L})       -> area({rectangle, L, L});
area(_)                 -> unknown.

% list notation
sum([H|T]) -> H + sum(T);
sum([]) -> 0.

% accumulator with list
average(X) -> average(X, 0, 0).

% note: following function has a different arity!
average([H|T], Count, Sum) -> average(T, Count+1, Sum+H);
average([], Count, Sum)    -> Sum / Count.

% map higher order function
% e.g., call with map(fun factorial/1, [1,2,3,4,5])
map(_, []) -> [];
map(F, [H|T]) -> [F(H) | map(F, T)].

% anonymous functions
double(L) -> map(fun(X) -> X*X end, L).

% map again, but with list comprehension
map2(F, L) -> [F(X) || X <- L].

% filter HOF, with case expression
filter(_, []) -> [];
filter(F, [H|T]) -> case F(H) of
                        true  -> [H | filter(F, T)];                            
                        false -> filter(F, T)
                    end.
