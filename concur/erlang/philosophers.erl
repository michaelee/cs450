-module(philosophers).
-export([start/0]).
-define(NUM_PHILOSOPHERS, 5).

leftFork(N) -> N.
rightFork(N) -> (N + 1) rem ?NUM_PHILOSOPHERS.

getFork(Footman, Fork) ->
    case forks:request(Footman, Fork) of
        granted -> ok;             
        unavailable -> io:format("Fork ~w unavailable~n", [Fork]),
                       timer:sleep(random:uniform(1000)),
                       getFork(Footman, Fork)
    end.

releaseFork(Footman, Fork) ->
    forks:release(Footman, Fork).

philosophize(_, _, 0) -> done;
philosophize(Id, Footman, NumMeals) ->
    getFork(Footman, leftFork(Id)),
    io:format("Philosopher ~w got fork ~w~n", [Id, leftFork(Id)]),
    getFork(Footman, rightFork(Id)),
    io:format("Philosopher ~w is eating!~n", [Id]),
    timer:sleep(random:uniform(1000)),
    releaseFork(Footman, leftFork(Id)),
    releaseFork(Footman, rightFork(Id)),
    philosophize(Id, Footman, NumMeals - 1).
    
start() ->
    Footman = forks:start(?NUM_PHILOSOPHERS),
    [ spawn(fun() -> philosophize(N, Footman, 500) end)
      || N <- lists:seq(0, ?NUM_PHILOSOPHERS - 1)].
