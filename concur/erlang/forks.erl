-module(forks).
-export([start/1, request/2, release/2]).

start(N) ->
    spawn(fun() -> loop(lists:seq(0,N-1)) end).

request(Pid, Fork) ->
    Pid ! {self(), {request, Fork}},
    receive
        {Pid, Msg} -> Msg
    end.

release(Pid, Fork) ->
    Pid ! {self(), {release, Fork}},
    receive
        {Pid, Msg} -> Msg
    end.

loop(Forks) ->
    receive
        {Pid, {request, Fork}} -> 
            case lists:member(Fork, Forks) of
                true ->
                    Pid ! {self(), granted},
                    loop(lists:delete(Fork, Forks));
                false ->
                    Pid ! {self(), unavailable},
                    loop(Forks)
            end;
        {Pid, {release, Fork}} ->
            Pid ! {self(), ok},
            loop([Fork|Forks]);
        {Pid, status} ->
            Pid ! {self(), Forks},
            loop(Forks);
        terminate ->
            ok                        
    end.
