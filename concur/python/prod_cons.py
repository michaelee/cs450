from __future__ import print_function
from threading import Semaphore, Thread
from collections import deque
from random import random
from time import sleep
import sys
import logging

def producer(n):
    for i in range(1, n+1):
        spaces.acquire()
        with mutex:
            buf.append(i)
        items.release()
        sleep(random())

def consumer(last_item):
    while True:
        items.acquire()
        with mutex:
            print('DEBUG:', buf)
            item = buf.popleft()
        spaces.release()
        print(item)
        sleep(random())
        if item == last_item:
            break

if __name__ == '__main__':
    if len(sys.argv) == 1:
        sys.exit()
    n = int(sys.argv[1])
    spaces = Semaphore(5)
    items  = Semaphore(0)
    mutex  = Semaphore(1)
    buf    = deque()
    p_t = Thread(target=producer, args=[n])
    p_t.start()
    sleep(1)  # let the queue pile up a bit
    c_t = Thread(target=consumer, args=[n])
    c_t.start()
