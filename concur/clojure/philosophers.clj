(require 'clojure.pprint)

(def running true)

(def num-philosophers 5)

;; forks are just refs to boolean values
(def forks
  (for [_ (range  num-philosophers)]
    (ref false)))

;; a philosopher has an id and a state
(defstruct philosopher :id :state)

;; philosophers are agents that wrap their state
(def philosophers
  (map #(agent (struct philosopher % :thinking))
       (range num-philosophers)))

;; reporter (see below)
(declare reporter)
(declare report-state)

;; Philosopher behavior ... controls transitions between thinking and eating
(defn philosophize [philosopher]
  (when running
    (send-off *agent* philosophize))
  (Thread/sleep (rand-int 500))
  (let [left-fork  (nth forks (:id philosopher))
        right-fork (nth forks (mod (inc (:id philosopher))
                                   num-philosophers))]
    (if (= :thinking (:state philosopher))
      (do (send reporter report-state (:id philosopher) :thinking)
          (dosync
           ;; check both forks
           (if (and (not @left-fork) (not @right-fork)) 
             (do (ref-set left-fork  true)
                 (ref-set right-fork true)
                 ;; thinking -> eating
                 (assoc philosopher :state :eating))
             ;; stay thinking (forks not available)
             (do (send reporter report-state (:id philosopher) :blocked)
                 philosopher))))
      (do (send reporter report-state (:id philosopher) :eating)
          (dosync
           ;; release forks
           (ref-set left-fork  false)
           (ref-set right-fork false)
           ;; eating -> sleeping
           (assoc philosopher :state :thinking))))))

;;; reporting information / functions

(defstruct phil-state-counts :blocked :eating :thinking)

;; reporter wraps a map of phil-id -> phil-state-counts
(def reporter
  (agent 
   (zipmap (range num-philosophers)
           (repeatedly #(struct phil-state-counts 0 0 0)))))

;; increments a state count for a given philosopher
(defn report-state [psc-map id state]
  (let [phil      (psc-map id)
        count     (phil state)
        next-phil (assoc phil state (inc count))]
    (assoc psc-map id next-phil)))

(defn start []
  (dorun
   (for [p philosophers]
     (send-off p philosophize))))

;; (pprint @reporter) ; to take "snapshot"

