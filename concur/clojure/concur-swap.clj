(ns cs450.concur-swap
  (:use clojure.pprint)
  (:import (java.util.concurrent Executors TimeUnit)))

(defn make-vecs [nvecs nitems]
  (map (comp ref vec)
       (partition nitems (range (* nvecs nitems)))))

(defn report [vec-refs]
  (let [vecs (map deref vec-refs)]
    (pprint vecs)
    (println "Unique items: "
             (count (distinct (apply concat vecs))))))

(defn rand-swap [vec-refs nvecs nitems]
  (let [v1ref (nth vec-refs (rand-int nvecs))
        idx1 (rand-int nitems)
        v2ref (nth vec-refs (rand-int nvecs))
        idx2 (rand-int nitems)]
    (dosync  ;; do the swap in a transaction
     (let [tmp (nth @v1ref idx1)]
       (alter v1ref assoc idx1 (nth @v2ref idx2))
       (alter v2ref assoc idx2 tmp)))))

(defn run [nvecs nitems nthreads niters]
  (let [vec-refs (make-vecs nvecs nitems)]
    (report vec-refs)
    (let [pool (Executors/newFixedThreadPool nthreads)]
      (dotimes [_ nthreads]
        (.execute pool #(dotimes [_ niters]
                          (rand-swap vec-refs nvecs nitems))))
      (.shutdown pool)
      (.awaitTermination pool 60 TimeUnit/SECONDS))
    (report vec-refs)))


;;; and now in idiomatic clojure
(defn concur-swap [nvecs nitems nthreads niters]
  (let [vec-refs (vec (map (comp ref vec)
                           (partition nitems (range (* nvecs nitems)))))
        rand-swap #(let [idx1 (rand-int nitems)
                         idx2 (rand-int nitems)
                         vec1 (nth vec-refs (rand-int nvecs))
                         vec2 (nth vec-refs (rand-int nvecs))]
                     (dosync
                      (let [tmp (nth @vec1 idx1)]
                        (alter vec1 assoc idx1 (nth @vec2 idx2))
                        (alter vec2 assoc idx2 tmp))))
        report #(do
                  (pprint (map deref vec-refs))
                  (println "Unique items:"
                           (count (distinct (apply concat
                                                   (map deref vec-refs))))))]
    (report)
    (dorun (apply pcalls (repeat nthreads #(dotimes [_ niters] (rand-swap)))))
    (report)))

;(concur-swap 10 10 10 1000)
;(shutdown-agents)