;;; agents

(def bond (agent 007))

(send bond inc)

(defn slow-inc [n]
  (Thread/sleep (rand-int 1000))
  ;; (println "Incrementing")
  (+ n 1))

(send bond slow-inc)

(defn multi-send [agt fn n-times]
  (dorun (apply pcalls
                (repeat n-times
                        #(send agt fn)))))

(defn multi-send-and-await [agt fn n-times]
  (multi-send agt fn n-times)
  (println "Done sending")
  (await agt)
  @agt)