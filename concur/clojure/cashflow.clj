(defstruct account :name :balance :min-balance)

;; atoms
(def basic-acc (atom (struct account "basic" 1000 0)))
(reset! basic-acc (struct account "basic" 500 0))
(swap!  basic-acc assoc :min-balance 50)

;; refs
(def checking (ref (struct account "checking" 250 0)))
(def saving   (ref (struct account "saving"   5000 2500)))

(defn sum-balances [& account-refs]
  (reduce + (map (comp :balance deref) account-refs)))

(defn transfer [from-ref to-ref amount]
  (dosync
   (let [new-from-bal (- (:balance @from-ref) amount)
	 new-to-bal   (+ (:balance @to-ref)   amount)]
     (when (>= new-from-bal (:min-balance @from-ref))
       (alter from-ref assoc :balance new-from-bal)
       (alter to-ref   assoc :balance new-to-bal)))))

(defn rand-transfer [& account-refs]
  (let [from-idx (rand-int (count account-refs))
	to-idx   (rand-int (count account-refs))]
    (if (= from-idx to-idx)
      (recur account-refs)
      (transfer (nth account-refs from-idx)
		(nth account-refs to-idx)
		(rand-int 500)))))

(defn run-transfers [nthreads ntransfers]
  (println "Before: " (sum-balances checking saving))
  (dorun
   (apply pcalls
	  (repeat nthreads
		  #(dotimes [_ ntransfers]
		     (rand-transfer checking saving)))))
  (println "After:  " (sum-balances checking saving)))

;; agents
(def investor (agent (struct account "funds" 10000 0)))

(defn invest [my-account amount to-account-ref]
  (dosync
   (let [to-bal (:balance @to-account-ref)]
     (Thread/sleep (rand-int 1000))  ; keep them in suspense
     (println "Investing " amount " from " my-account)
     (alter to-account-ref assoc :balance (+ to-bal amount))))
  (assoc my-account :balance (- (:balance my-account) amount)))

(defn invest-till-dry [my-account to-account]
  (if (> (:balance my-account) (:min-balance my-account))
    (do (send *agent* invest-till-dry to-account)
	(invest my-account
		(inc (rand-int (- (:balance my-account)
				  (:min-balance my-account))))
		to-account))
    my-account))

(def investors (map #(agent (struct account % 1000000 0))
		    (range 10)))

(def startup (ref (struct account "money sink" 0 0)))

(defn total-cash []
  (+ (:balance @startup)
     (reduce + (map (comp :balance deref) investors))))

(defn run-investment-round []
  (dorun (map #(send % invest-till-dry startup) investors)))