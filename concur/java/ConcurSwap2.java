import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.locks.*;

public class ConcurSwap2 {
    int numLists;
    int numItemsPerList;
    int numThreads;
    int numIterations;

    private List<List<Integer>> sharedData;
    private List<Lock> locks;
    private ExecutorService threadPool;
    private Random randGen;

    public ConcurSwap2 (int nLists, int nItems, int nThreads, int nIters) {
	numLists = nLists;
	numItemsPerList = nItems;
	numThreads = nThreads;
	numIterations = nIters;
        randGen = new Random();
        sharedData = new ArrayList<List<Integer>>(numLists);
        locks = new ArrayList<Lock>(numLists);
        for (int i=0, val=0; i<numLists; i++) {
            List<Integer> l = Collections.synchronizedList(
                                  new ArrayList<Integer>(numItemsPerList));
            for (int j=0; j<numItemsPerList; j++) {
                l.add(val++);
            }
            sharedData.add(l);
            locks.add(new ReentrantLock());
            threadPool = Executors.newFixedThreadPool(numThreads);
        }
    }

    public void addSwapper () {
        threadPool.execute(new Swapper());
    }

    public void report () {
        Set<Integer> uniquer = new HashSet<Integer>();
        for (List<Integer> l : sharedData) {
            System.out.println(l.toString());
            uniquer.addAll(l);
        }
        System.out.printf("Unique items: %d\n", uniquer.size());
    }

    public void await () {
        try {
            threadPool.shutdown();
            threadPool.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    class Swapper implements Runnable {
        public void run () {
            for (int i=0; i<numIterations; i++) {
                int idx1 = randGen.nextInt(numItemsPerList),
                    idx2 = randGen.nextInt(numItemsPerList);
                int lidx1 = randGen.nextInt(numLists),
                    lidx2 = randGen.nextInt(numLists);
                List<Integer> lst1 = sharedData.get(lidx1),
                              lst2 = sharedData.get(lidx2);
                Lock lock1 = locks.get(lidx1),
                     lock2 = locks.get(lidx2);

                lock1.lock();
                lock2.lock();
                try {
                    int tmpVal = lst1.get(idx1);
                    lst1.set(idx1, lst2.get(idx2));
                    lst2.set(idx2, tmpVal);
                } finally {
                    lock1.unlock();
                    lock2.unlock();
                }
            }
        }
    }

    public static void main (String[] args) {
	int nLists   = Integer.parseInt(args[0]),
	    nItems   = Integer.parseInt(args[1]),
	    nThreads = Integer.parseInt(args[2]),
	    nIters   = Integer.parseInt(args[3]);
        ConcurSwap2 syncTest = new ConcurSwap2(nLists, nItems, nThreads, nIters);
        syncTest.report();
        for (int i=0; i<nThreads; i++) {
            syncTest.addSwapper();
        }
        syncTest.await();
        syncTest.report();
    }
}
